FROM condaforge/miniforge3 as conda

ENV CPPTANGO_VERSION 9.3.5
ENV TANGO_TEST_VERSION 3.4

RUN conda create -y -p /tango \
    cpptango=$CPPTANGO_VERSION \
    tango-test=$TANGO_TEST_VERSION \
  && conda clean -afy

COPY copy-bin-deps.sh /

# Only keep required binaries and their shared library dependencies
RUN /copy-bin-deps.sh /tango TangoTest /tango-slim \
   && rm -rf /tango \
   && mv /tango-slim /tango

FROM debian:11-slim

LABEL maintainer="TANGO Controls Team <contact@tango-controls.org>"

COPY --from=conda /tango /tango

ENV PATH /tango/bin:$PATH

RUN useradd -m tango
USER tango

CMD /tango/bin/TangoTest test
