# tango-test docker image

tango-test [docker] image to run the [TangoTest device server](https://gitlab.com/tango-controls/TangoTest).

## Usage

To use this image, you need a working instance of the Tango Database running.
The recommended way is to use `docker-compose` to start both mysql and tango-db containers:

```yaml
version: '3.7'
services:
  mysql:
    image: registry.gitlab.com/tango-controls/docker/mysql
    environment:
     - MYSQL_ROOT_PASSWORD=root
  tango-db:
    image: registry.gitlab.com/tango-controls/docker/tango-db
    ports:
     - "10000:10000"
    environment:
     - TANGO_HOST=localhost:10000
     - MYSQL_HOST=mysql:3306
     - MYSQL_USER=tango
     - MYSQL_PASSWORD=tango
     - MYSQL_DATABASE=tango
    depends_on:
     - mysql
  tango-test:
    image: registry.gitlab.com/tango-controls/docker/tango-test
    restart: always
    environment:
     - TANGO_HOST=tango-db:10000
    depends_on:
     - tango-db
```

Run `docker-compose up` to start the 3 containers.

[docker]: https://www.docker.com
